module.exports = function (grunt) {

  // load tasks
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-rename');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-postcss');

  // config
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    // concat
    concat: {
      css: {
        src: ['src/css/*.css'],
        dest: 'dist/css/<%= pkg.name %>.css'
      },
      js: {
        src: ['src/js/*.js'],
        dest: 'dist/js/<%= pkg.name %>.js'
      }
    },

    // watch
    watch: {
      files: ['Gruntfile.js', 'src/js/*.js', 'src/css/*.css'],
      tasks: ['concat', 'uglify', 'clean', 'postcss', 'rename']
    },

    // uglify
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'dist/js/<%= pkg.name %>.min.js': ['<%= concat.js.dest %>']
        }
      }
    },

    // clean
    clean: {
      js: ['dist/js/*.js', '!dist/js/*.min.js']
    },

    // postcss
    postcss: {
      options: {
        // map: true, // inline sourcemaps
        // or
        /*map: {
            inline: false, // save all sourcemaps as separate files...
            annotation: 'dist/css/maps/' // ...to the specified directory
        },*/
        processors: [
          require('autoprefixer-core')({
            browsers: 'last 2 versions'
          }),
          require('cssnano')()
        ]
      },
      dist: {
        src: 'dist/css/*.css'
      }
    },

    // rename
    rename: {
      main: {
        files: [
          {
            src: ['dist/css/<%= pkg.name %>.css'],
            dest: 'dist/css/<%= pkg.name %>.min.css'
          }
        ]
      }
    }

  });

  // registers default task
  grunt.registerTask('default', ['concat', 'uglify', 'clean', 'postcss', 'rename', 'watch']);

};